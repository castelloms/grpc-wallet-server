FROM openjdk:9-jre
WORKDIR /wallet-server
COPY ./target/grpc-wallet-server.jar /wallet/app.jar
CMD ["/usr/bin/java","-jar","/wallet/app.jar"]
