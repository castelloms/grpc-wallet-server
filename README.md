# GRPC Wallet Server
Project Wallet Server

## Prerequisites

 - JDK 9+
 - Docker 18.02.0+ (docker-compose 1.22.0+)

## Initialize Project

**Clone project**

```sh
git clone https://bitbucket.org/castelloms/grpc-wallet-server.git
```

**Execute this commands on the project folder - Docker**

```sh
In the folder where is pom.xml, execute:
- generate-image.sh - build project with your dependencies and generate docker image

In the docker folder, execute:
- docker-compose up - initialize all containers declared in the file
```

**Execute this commands on the project folder - Local**

```sh
In the folder where is application-dev.yml, replace:
- url: jdbc:mysql://<host>:<port>/<schema>?useSSL=false&allowPublicKeyRetrieval=true
- username: <username>
- password: <password>

In the folder where is pom.xml, execute:
- mvn clean install - build project with your dependencies
- java -jar target/grpc-wallet-server.jar --spring.profiles.active=dev - start the project grpc listening on port 8088
```

 