package com.castello.walletserver.enumeration;

public enum Operation {

    BALANCE, DEPOSIT, WITHDRAW

}
