package com.castello.walletserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.castello.walletserver.entity.Balance;
import com.castello.walletserver.entity.BalancePk;
import com.castello.walletserver.grpc.Currency;

@Repository
public interface BalanceRepository extends JpaRepository<Balance, BalancePk> {

    List<Balance> findByBalancePkUserId(String userId);

    Optional<Balance> findByBalancePkUserIdAndBalancePkCurrency(String userId, Currency currency);

}
