package com.castello.walletserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.castello.walletserver.entity.Wallet;

@Repository
public interface TransactionRepository extends JpaRepository<Wallet, String> {

}
