package com.castello.walletserver.exception;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;

public class InsufficientFundsException extends StatusRuntimeException {

    private static final long serialVersionUID = 6206686637973862962L;

    public InsufficientFundsException(Status status) {
        super(status.withDescription("Insufficient funds"));
    }

}
