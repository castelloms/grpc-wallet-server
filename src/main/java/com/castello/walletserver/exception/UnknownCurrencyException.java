package com.castello.walletserver.exception;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;

public class UnknownCurrencyException extends StatusRuntimeException {

    private static final long serialVersionUID = -608464187062734838L;

    public UnknownCurrencyException(Status status) {
        super(status.withDescription("Unknown currency"));
    }

}
