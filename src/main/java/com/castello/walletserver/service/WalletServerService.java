package com.castello.walletserver.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.castello.walletserver.entity.Balance;
import com.castello.walletserver.entity.BalancePk;
import com.castello.walletserver.entity.Wallet;
import com.castello.walletserver.enumeration.Operation;
import com.castello.walletserver.exception.InsufficientFundsException;
import com.castello.walletserver.grpc.BalanceResponse;
import com.castello.walletserver.grpc.DepositWithdrawRequest;
import com.castello.walletserver.grpc.TransactionPerSecondResponse;
import com.castello.walletserver.repository.BalanceRepository;
import com.castello.walletserver.repository.TransactionRepository;

import io.grpc.Status;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class WalletServerService {

    private final BalanceRepository balanceRepository;
    private final TransactionRepository transactionRepository;

    public List<BalanceResponse> balance(String userId) {
        return balanceRepository.findByBalancePkUserId(userId)
                .stream()
                .map(Balance::toResponse)
                .collect(Collectors.toList());
    }

    public String deposit(DepositWithdrawRequest depositRequest) {
        balanceRepository
                .findByBalancePkUserIdAndBalancePkCurrency(depositRequest.getUserId(), depositRequest.getCurrency())
                .ifPresentOrElse(balance -> {
                    balance.setAmount(balance.getAmount() + depositRequest.getAmount());
                    balanceRepository.save(balance);
                }, () -> {
                    BalancePk balancePk = BalancePk.builder()
                            .userId(depositRequest.getUserId())
                            .currency(depositRequest.getCurrency())
                            .build();
                    balanceRepository.save(Balance.builder()
                            .balancePk(balancePk)
                            .amount(depositRequest.getAmount())
                            .build());
                });

        return transactionRepository.save(Wallet.builder()
                .userId(depositRequest.getUserId())
                .currency(depositRequest.getCurrency())
                .amount(depositRequest.getAmount())
                .operation(Operation.DEPOSIT)
                .dateTimeOperation(LocalDateTime.now())
                .build()).getId();
    }

    public String withdraw(DepositWithdrawRequest withdrawRequest) {
        balanceRepository
                .findByBalancePkUserIdAndBalancePkCurrency(withdrawRequest.getUserId(), withdrawRequest.getCurrency())
                .ifPresent(balance -> {
                    if (balance.getAmount() >= withdrawRequest.getAmount()) {
                        balance.setAmount(balance.getAmount() - withdrawRequest.getAmount());
                        balanceRepository.save(balance);
                    } else {
                        throw new InsufficientFundsException(Status.FAILED_PRECONDITION);
                    }
                });
        return transactionRepository.save(Wallet.builder()
                .userId(withdrawRequest.getUserId())
                .currency(withdrawRequest.getCurrency())
                .amount(withdrawRequest.getAmount())
                .operation(Operation.WITHDRAW)
                .dateTimeOperation(LocalDateTime.now())
                .build()).getId();
    }

    public List<TransactionPerSecondResponse> tps() {
        List<TransactionPerSecondResponse> tps = new ArrayList<>();
        transactionRepository.findAll().stream()
                .collect(Collectors.groupingBy(Wallet::getDateTimeOperation, Collectors.counting()))
                .forEach((key, value) -> {
                    TransactionPerSecondResponse tpsResponse = TransactionPerSecondResponse.newBuilder()
                            .setCount(value.intValue())
                            .setTime(key.toString())
                            .build();
                    tps.add(tpsResponse);
                });
        return tps;
    }

}
