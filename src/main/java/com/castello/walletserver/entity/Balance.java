package com.castello.walletserver.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Version;

import com.castello.walletserver.grpc.BalanceProto;
import com.castello.walletserver.grpc.BalanceResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
public class Balance implements Serializable {

    private static final long serialVersionUID = 8626636324623308715L;

    @EmbeddedId
    private BalancePk balancePk;

    private double amount;

    @Version
    private Integer version;

    public BalanceResponse toResponse() {
        BalanceProto balance = BalanceProto.newBuilder()
                .setAmount(amount)
                .setCurrency(balancePk.getCurrency())
                .build();
        return BalanceResponse.newBuilder()
                .mergeBalance(balance)
                .build();
    }

}
