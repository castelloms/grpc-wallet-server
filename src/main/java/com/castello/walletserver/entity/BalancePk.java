package com.castello.walletserver.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.castello.walletserver.grpc.Currency;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Embeddable
public class BalancePk implements Serializable {

    private static final long serialVersionUID = 6262638620727696086L;

    private String userId;

    @Enumerated(EnumType.STRING)
    private Currency currency;

}
