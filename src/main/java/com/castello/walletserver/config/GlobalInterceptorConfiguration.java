package com.castello.walletserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import com.castello.walletserver.interceptor.LogGrpcInterceptor;

import net.devh.boot.grpc.server.interceptor.GlobalServerInterceptorConfigurer;

@Order
@Configuration
public class GlobalInterceptorConfiguration {

    @Bean
    public GlobalServerInterceptorConfigurer globalInterceptorConfigurerAdapter() {
        return registry -> registry.addServerInterceptors(new LogGrpcInterceptor());
    }

}
