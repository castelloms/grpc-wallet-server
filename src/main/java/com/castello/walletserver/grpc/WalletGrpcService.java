package com.castello.walletserver.grpc;

import java.util.List;

import com.castello.walletserver.grpc.WalletServerServiceGrpc.WalletServerServiceImplBase;
import com.castello.walletserver.service.WalletServerService;
import com.google.protobuf.Empty;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;

@RequiredArgsConstructor
@GrpcService
public class WalletGrpcService extends WalletServerServiceImplBase {

    private final WalletServerService walletServerService;

    @Override
    public void deposit(DepositWithdrawRequest request, StreamObserver<DepositWithdrawResponse> responseObserver) {
        try {
            String id = walletServerService.deposit(request);
            responseObserver.onNext(DepositWithdrawResponse.newBuilder().setId(id).build());
            responseObserver.onCompleted();
        } catch (StatusRuntimeException e) {
            responseObserver.onError(new StatusRuntimeException(e.getStatus()));
        }
    }

    @Override
    public void withdraw(DepositWithdrawRequest request, StreamObserver<DepositWithdrawResponse> responseObserver) {
        try {
            String id = walletServerService.withdraw(request);
            responseObserver.onNext(DepositWithdrawResponse.newBuilder().setId(id).build());
            responseObserver.onCompleted();
        } catch (StatusRuntimeException e) {
            responseObserver.onError(new StatusRuntimeException(e.getStatus()));
        }
    }

    @Override
    public void balance(BalanceRequest request, StreamObserver<BalanceResponse> responseObserver) {
        try {
            List<BalanceResponse> balances = walletServerService.balance(request.getUserId());
            balances.forEach(responseObserver::onNext);
            responseObserver.onCompleted();
        } catch (StatusRuntimeException e) {
            responseObserver.onError(new StatusRuntimeException(e.getStatus()));
        }
    }

    @Override
    public void tps(Empty request, StreamObserver<TransactionPerSecondResponse> responseObserver) {
        try {
            List<TransactionPerSecondResponse> tps = walletServerService.tps();
            tps.forEach(responseObserver::onNext);
            responseObserver.onCompleted();
        } catch (StatusRuntimeException e) {
            responseObserver.onError(new StatusRuntimeException(e.getStatus()));
        }
    }

}
