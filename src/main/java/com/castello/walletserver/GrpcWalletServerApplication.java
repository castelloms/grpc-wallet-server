package com.castello.walletserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrpcWalletServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrpcWalletServerApplication.class, args);
    }

}