package com.castello.walletserver;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackageClasses = GrpcWalletServerApplication.class)
public class GrpcWalletServerApplicationTest {

    public static void main(String[] args) {
        SpringApplication.run(GrpcWalletServerApplicationTest.class, args);
    }

}
