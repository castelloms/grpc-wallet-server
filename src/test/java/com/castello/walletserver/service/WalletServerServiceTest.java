package com.castello.walletserver.service;

import java.util.List;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.castello.walletserver.GrpcWalletServerApplicationTest;
import com.castello.walletserver.container.MySqlContainerTest;
import com.castello.walletserver.exception.InsufficientFundsException;
import com.castello.walletserver.grpc.BalanceResponse;
import com.castello.walletserver.grpc.Currency;
import com.castello.walletserver.grpc.DepositWithdrawRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GrpcWalletServerApplicationTest.class)
public class WalletServerServiceTest {

    @ClassRule
    public static final MySqlContainerTest CONTAINER = new MySqlContainerTest();

    @Autowired
    private WalletServerService walletServerService;

    @BeforeClass
    public static void init() {
        CONTAINER.start();
    }

    @AfterClass
    public static void teardown() {
        CONTAINER.stop();
    }

    @Test
    public void balanceCorrectTest() {
        String userId = UUID.randomUUID().toString();
        DepositWithdrawRequest depositWithdrawRequest = DepositWithdrawRequest.newBuilder()
                .setUserId(userId)
                .setAmount(500)
                .setCurrency(Currency.USD)
                .build();
        walletServerService.deposit(depositWithdrawRequest);
        depositWithdrawRequest = DepositWithdrawRequest.newBuilder(depositWithdrawRequest)
                .setUserId(userId)
                .setAmount(100)
                .setCurrency(Currency.EUR)
                .build();
        walletServerService.deposit(depositWithdrawRequest);
        depositWithdrawRequest = DepositWithdrawRequest.newBuilder(depositWithdrawRequest)
                .setCurrency(Currency.USD)
                .setAmount(200)
                .build();
        walletServerService.withdraw(depositWithdrawRequest);
        List<BalanceResponse> balances = walletServerService.balance(userId);
        Double balanceUsd = balances.stream()
                .filter(x -> x.getBalance().getCurrency().equals(Currency.USD))
                .findFirst()
                .map(x -> x.getBalance().getAmount())
                .get();
        Assert.assertEquals(300, balanceUsd, 0);
        Double balanceEur = balances.stream()
                .filter(x -> x.getBalance().getCurrency().equals(Currency.EUR))
                .findFirst()
                .map(x -> x.getBalance().getAmount())
                .get();
        Assert.assertEquals(100, balanceEur, 0);
    }

    @Test(expected = InsufficientFundsException.class)
    public void withdrawWithInsufficientFundsExceptionTest() {
        DepositWithdrawRequest depositWithdrawRequest = DepositWithdrawRequest.newBuilder()
                .setUserId(UUID.randomUUID().toString())
                .setAmount(100)
                .setCurrency(Currency.USD)
                .build();
        walletServerService.deposit(depositWithdrawRequest);
        depositWithdrawRequest = DepositWithdrawRequest.newBuilder(depositWithdrawRequest)
                .setAmount(200)
                .build();
        walletServerService.withdraw(depositWithdrawRequest);
    }

}
