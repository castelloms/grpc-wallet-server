package com.castello.walletserver.container;

import static com.github.dockerjava.api.model.Ports.Binding.bindPort;

import org.testcontainers.containers.MySQLContainer;

import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.PortBinding;

public class MySqlContainerTest extends MySQLContainer<MySqlContainerTest> {

    public MySqlContainerTest() {
        withPassword("wallet");
        withDatabaseName("wallet");
        withCreateContainerCmdModifier(consumer -> {
            consumer.withName("mysql-wallet");
            consumer.withPortBindings(new PortBinding(bindPort(MYSQL_PORT), new ExposedPort(MYSQL_PORT)));
        });
    }

}
